object Fibonacci {
  def main(args: Array[String]): Unit = {
    println("Hello world!")
  }

  def fib(n: Int): Int = n match {
    case 1 | 2 => 1
    case _ => fib(n - 1) + fib (n - 2)
  }
}
